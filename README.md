# Vendedores ¡a correr!

La empresa “Imágenes del mundo” ha realizado un concurso para sus vendedores, ¡una carrera para encontrar las imágenes más interesantes de la web!.

¿En qué consiste?

Los vendedores de la empresa “Imágenes del mundo” deben traer de la web imágenes relacionadas a una palabra que el usuario ingresará en la aplicación, la imagen que más le guste al usuario ganará 3 puntos, el vendedor que complete 20 puntos ganará la carrera, el codiciado premio es la posibilidad de crear una factura en Alegra con los puntos acumulados por todos los vendedores durante la carrera.

### ¿Cómo funciona?

- El usuario debe ingresar en la aplicación la palabra sobre la cual desea consultar imagenes.
- Cada vendedor debe mostrar una imagen al usuario basándose en la palabra que el usuario ingrese como criterio de búsqueda.
- El usuario debe seleccionar la imagen que más le guste con esto se le otorgarán 3 puntos al vendedor.
- Los vendedores deben seguir este ciclo hasta que algun vendedor acumule 20 puntos, esto marca el fin de la carrera.
- Cuando un vendedor gane la carrera, debe crear una factura en Alegra indicando en la cantidad vendida los puntos acumulados por todos los participantes. La factura creada debe quedar asociada al vendedor ganador. El cliente y producto asociado puede seleccionarse a consideración del candidato.

La empresa “Imágenes del mundo” es la mejor empresa del mundo de imagenes, sus vendedores son los mejores por lo tanto esperan de cualquier trabajo perfección, algo de menor calidad será automáticamente descalificado por nuestros robots de control de calidad. Esperamos ser deslumbrados.

### Consideraciones

- El tiempo para realizar la prueba es de 1 semana.
- Los vendedores deben consultarse de una cuenta de Alegra creada por el candidato.
- Toda la comunicación con Alegra se debe hacer por medio de la API (https://developer.alegra.com/)
- La interfaz debe contener como mínimo: los puntos de cada vendedor, los puntos que le hacen falta para ganar, la información de la factura que se creó al ganar la carrera
- Las imágenes se pueden consultar utilizando el API de Google Images.
- Se debe enviar una url donde se pueda visitar la herramienta desarrollada.
- El código se debe alojar en Bitbucket en un repositorio de git privado. Se debe dar acceso al correo scouting@alegra.com.
- La aplicación debe ser realizada en alguno de los siguientes frameworks: React, VueJs o Angular
- Se debe enviar también el correo y la contraseña de la cuenta de Alegra con la cual se hace la integración.
