import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { isEmpty } from '../utils/helpers';

const Container = styled.div``;
const InnerContainer = styled.div`
  margin: 0 auto;
  width: 95%;
  max-width: 1360px;
  border: 1px solid gray;
  padding: 2rem;
  @media (min-width: 640px) {
    width: 75%;
  }
  @media (min-width: 1024px) {
    width: 50%;
  }
`;
const NameValue = styled.p`
  display: block;
  color: #454545;
  & .name {
    display: inline-block;
    width: 12rem;
    font-weight: 700;
    margin: 0 0.5rem 0 0;
  }
  & .total::after {
    content: '$';
  }
`;

const Title = styled.h4`
  margin: 0 0 2rem 0;
`;

const WrapperButtonReset = styled.div`
  display: flex;
  justify-content: center;
  padding: 1rem 0;
`;
const ButtonReset = styled.button`
  display: inline-block;
  background-color: #00b675;
  color: white;
  text-transform: uppercase;
  font-weight: 600;
  padding: 0.5rem 1.2rem;
  cursor: pointer;
`;

const Invoice = ({ invoice, resetInvoice }) => {
  if (isEmpty(invoice)) return null;
  const onReset = () => resetInvoice(true);
  return (
    <Container>
      <InnerContainer>
        <Title>se ha creado una factura con los siguientes valores:</Title>
        <NameValue>
          <span className="name">N° de factura:</span>
          <span className="value">{invoice.id}</span>
        </NameValue>
        <NameValue>
          <span className="name">Fecha:</span>
          <span className="value">{invoice.date}</span>
        </NameValue>
        <NameValue>
          <span className="name">Fecha de vencimiento:</span>
          <span className="value">{invoice.dueDate}</span>
        </NameValue>
        <NameValue>
          <span className="name">Cliente:</span>
          <span className="value">{invoice.client.name}</span>
        </NameValue>
        <NameValue>
          <span className="name">Vendedor:</span>
          <span className="value">{invoice.seller.name}</span>
        </NameValue>
        <NameValue>
          <span className="name">Productos:</span>
          <span className="value">{invoice.items[0].name}</span>
        </NameValue>
        <NameValue>
          <span className="name">total:</span>
          <span className="value total">{invoice.total}</span>
        </NameValue>

        <WrapperButtonReset>
          <ButtonReset onClick={onReset}>nueva búsqueda</ButtonReset>
        </WrapperButtonReset>
      </InnerContainer>
    </Container>
  );
};

Invoice.propTypes = {
  invoice: PropTypes.object.isRequired,
  resetInvoice: PropTypes.func.isRequired,
};

export default Invoice;
