import React from 'react';
import styled, { keyframes } from 'styled-components';
import PropTypes from 'prop-types';

const bouncingLoader = keyframes`
  from {
    opacity: 1;
    transform: translateY(0);
  }
  to {
    opacity: 0.1;
    transform: translateY(-1rem);
  }
`;

const BouncingLoader = styled.div`
  display: flex;
  justify-content: center;
  & > div {
    width: 1rem;
    height: 1rem;
    margin: 3rem 0.2rem;
    background: #00b675;
    border-radius: 50%;
    animation: ${bouncingLoader} 0.6s infinite alternate;
  }
  &>div: nth-child(2){
    animation-delay: 0.2s;
  }
  &>div: nth-child(3){
    animation-delay: 0.4s;
  }
}
`;

const Message = styled.p`
  text-align: center;
`;

const Spinner = ({ message }) => (
  <div>
    <BouncingLoader>
      <div />
      <div />
      <div />
    </BouncingLoader>
    <Message>{message}</Message>
  </div>
);

Spinner.propTypes = {
  message: PropTypes.string,
};

export default Spinner;
