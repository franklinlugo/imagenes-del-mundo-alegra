import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { FaUser } from 'react-icons/fa';

const Container = styled.div`
  width: 200px;
`;

const Name = styled.div`
  display: flex;
  align-items: center;
  & > .text {
    margin: 0 0 0 1rem;
  }
`;

const Image = styled.img`
  width: 200px;
  height: 106px;
`;
const Seller = ({ name, points, sales, image, id, onSellProduct }) => {
  const onImageClick = () => {
    onSellProduct(id);
  };
  return (
    <Container>
      <Name>
        <FaUser />
        <span className="text">{name}</span>
      </Name>
      puntos: {points}
      <Image src={image} alt="" onClick={onImageClick} />
    </Container>
  );
};

Seller.propTypes = {
  name: PropTypes.string.isRequired,
  points: PropTypes.number.isRequired,
  sales: PropTypes.array,
  image: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  onSellProduct: PropTypes.func.isRequired,
};

export default Seller;
