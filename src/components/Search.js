import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const Container = styled.div`
  display: flex;
  justify-content: center;
  padding: 0.5rem 0;
`;

const Input = styled.input`
  width: 100%;
  text-align: center;
  border: 1.5px solid #00b675;
  background-color: #fff;
  height: 2.5rem;
  margin: 0 0 0 0;
  @media (min-width: 640px) {
    width: 50%;
  }
`;

const Search = ({ onSearch, value }) => (
  <Container>
    <Input name="search" placeholder="BUSCAR IMÁGENES" onChange={onSearch} value={value} />
  </Container>
);

Search.propTypes = {
  onSearch: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
};

export default Search;
