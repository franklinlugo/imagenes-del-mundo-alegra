import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import Seller from './Seller';

const Container = styled.div`
  width: 70%;
  margin: 0 auto;
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
  grid-gap: 2rem;
  justify-items: center;
  justify-content: center;
`;

const Sellers = ({ sellers, onSellProduct }) => {
  if (sellers.length === 0) return null;
  return (
    <Container>
      {Object.keys(sellers).map(key => (
        <Seller {...sellers[key]} onSellProduct={onSellProduct} />
      ))}
    </Container>
  );
};

Sellers.propTypes = {
  sellers: PropTypes.shape({
    name: PropTypes.string,
    points: PropTypes.number,
    sales: PropTypes.array,
    image: PropTypes.string,
    id: PropTypes.number,
  }).isRequired,
  onSellProduct: PropTypes.func.isRequired,
};

export default Sellers;
