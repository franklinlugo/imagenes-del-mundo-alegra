const invoice = {
  id: '3',
  date: '2018-10-17',
  dueDate: '2018-10-24',
  observations: null,
  anotation: null,
  termsConditions:
    'Esta factura se asimila en todos sus efectos a una letra de cambio de conformidad con el Art. 774 del código de comercio. Autorizo que en caso de incumplimiento de esta obligación sea reportado a las centrales de riesgo, se cobraran intereses por mora.',
  status: 'draft',
  client: {
    id: '1',
    name: 'Franklin Lugo',
    identification: '17537108',
    phonePrimary: '',
    phoneSecondary: '',
    fax: '',
    mobile: '',
    email: '',
    address: {
      address: '',
      city: '',
    },
  },
  numberTemplate: {
    id: '1',
    prefix: null,
    number: '3',
    text: null,
  },
  total: 1,
  totalPaid: 0,
  balance: 1,
  decimalPrecision: '0',
  warehouse: {
    id: '1',
    name: 'Principal',
  },
  seller: {
    id: '2',
    name: 'Dave Murray',
    identification: 'UrFeEz6IHO',
    observations: 'Lead guitar',
  },
  priceList: null,
  items: [
    {
      id: 1,
      name: 'producto',
      description: null,
      price: 1,
      discount: '',
      reference: null,
      quantity: 1,
      tax: [],
      total: 1,
    },
  ],
};

export default invoice;
