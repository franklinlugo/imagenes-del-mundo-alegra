import { buildUrl } from './helpers';

export async function getSellers() {
  const authorization = window.btoa(`${process.env.REACT_APP_USER_EMAIL}:${process.env.REACT_APP_USER_TOKEN}`);
  try {
    const response = await fetch('https://app.alegra.com/api/v1/sellers/', {
      headers: { Authorization: `Basic ${authorization}` },
    });
    const sellers = await response.json();
    return sellers;
  } catch (error) {
    console.error('there was an error ->', error);
  }
}

export async function search(query) {
  const URL = 'https://www.googleapis.com/customsearch/v1';
  try {
    let images = [];
    // get 50 results of google images, max 100
    for (let index = 1; index <= 50; index += 10) {
      const params = {
        key: process.env.REACT_APP_GOOGLE_API_KEY,
        cx: process.env.REACT_APP_SEARCH_ENGINE_ID,
        searchType: 'image',
        start: index,
        // start: 1,
        q: query,
      };
      const _url = buildUrl(URL, params);
      let response = await fetch(_url, { method: 'GET' });
      let results = await response.json();
      images.push(results.items);
    }
    return images;
  } catch (error) {
    console.error('there was an error on searching ->', error);
  }
}

export async function createInvoice(date, dueDate, client, items, seller) {
  const URL = 'https://app.alegra.com/api/v1/invoices';
  const authorization = window.btoa(`${process.env.REACT_APP_USER_EMAIL}:${process.env.REACT_APP_USER_TOKEN}`);
  const body = {
    date,
    dueDate,
    client,
    items,
    seller,
  };
  try {
    const response = await fetch(URL, {
      headers: { Authorization: `Basic ${authorization}` },
      method: 'POST',
      body: JSON.stringify(body),
    });
    const result = response.json();
    return result;
  } catch (error) {
    console.error('there was an error creating the invoice ->', error);
  }
}

const api = {
  getSellers,
  search,
  createInvoice,
};

export default api;
