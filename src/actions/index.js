import { RESULTS, SET_STATUS, GET_SELLERS, CREATE_INVOICE, RESET_INVOICE } from './types';
import api from '../utils/api';

export const search = query => async dispatch => {
  try {
    const results = await api.search(query);
    dispatch(onSuccess(results));
    dispatch(setStatus('fetched'));
  } catch (error) {
    console.error('There was an error doing the search', error);
    dispatch(setStatus('error'));
  }
};

const onSuccess = results => ({
  type: RESULTS,
  results: results,
});

export const setStatus = (status, message = '') => ({
  type: SET_STATUS,
  status: {
    status,
    message,
  },
});

export const getSellers = () => async dispatch => {
  try {
    const sellers = await api.getSellers();
    dispatch({
      type: GET_SELLERS,
      sellers,
    });
  } catch (error) {
    console.error('There was an error getting the sellers list', error);
    dispatch(setStatus('error'));
  }
};

export const createInvoice = (date, dueDate, client, items, seller) => async dispatch => {
  try {
    dispatch(setStatus('fetching', '...creando factura'));
    const invoice = await api.createInvoice(date, dueDate, client, items, seller);
    dispatch(createInvoiceSuccess(invoice));
    dispatch(setStatus('fetched'));
  } catch (error) {
    console.error('There was an error creating an  invoice', error);
    dispatch(setStatus('error'));
  }
};

const createInvoiceSuccess = invoice => ({
  type: CREATE_INVOICE,
  invoice,
});

export const resetInvoice = () => ({
  type: RESET_INVOICE,
});
