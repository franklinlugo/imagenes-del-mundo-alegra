export const RESULTS = 'RESULTS';
export const SET_STATUS = 'SET_STATUS';
export const GET_SELLERS = 'GET_SELLERS';
export const CREATE_INVOICE = 'CREATE_INVOICE';
export const RESET_INVOICE = 'RESET_INVOICE';
