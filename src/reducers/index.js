import { combineReducers } from 'redux';
import { RESULTS, SET_STATUS, GET_SELLERS, CREATE_INVOICE, RESET_INVOICE } from '../actions/types';

const results = (state = [], action) => {
  const { type, results } = action;
  if (type === RESULTS) return results;
  return state;
};

const status = (state = {}, action) => {
  const { type, status } = action;
  if (type === SET_STATUS) return status;
  return state;
};

const sellers = (state = [], action) => {
  const { type, sellers } = action;
  if (type === GET_SELLERS) return sellers;
  return state;
};

const invoice = (state = {}, action) => {
  const { type, invoice } = action;
  if (type === CREATE_INVOICE) return invoice;
  if (type === RESET_INVOICE) return {};
  return state;
};

const reducer = combineReducers({
  results,
  status,
  sellers,
  invoice,
});

export default reducer;
