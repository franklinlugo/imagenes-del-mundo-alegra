import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import moment from 'moment';
import { search, getSellers, setStatus, createInvoice, resetInvoice } from '../actions';
// import { createInvoice } from '../actions/invoice';
import _ from 'lodash';
import { bindActionCreators } from 'redux';
import Spinner from '../components/Spinner';

import Search from '../components/Search';
import Sellers from '../components/Sellers';
import Invoice from '../components/Invoice';

// import images from '../utils/images'; // data for dev purposes

const Main = styled.div``;

const Container = styled.div`
  margin: 0 auto;
  width: 100%;
  max-width: 1440px;
`;

class App extends Component {
  state = {
    searchTerm: '',
    sellers: {},
  };

  imageList = [];
  delayedSearch = _.debounce(this.props.search, 700);

  onChangeInputSearch = event => {
    this.setState({
      searchTerm: event.target.value,
    });
    this.doSearch();
  };

  doSearch = () => {
    this.props.setStatus('fetching', '...Buscando');
    this.delayedSearch(this.state.searchTerm);
  };

  componentDidMount() {
    this.props.getSellers();
    // this.filterImagesLink(images); //for dev purposes
  }

  componentDidUpdate(prevProps) {
    if (this.props.results !== prevProps.results) {
      this.filterImagesLink(this.props.results);
      this.addSellers(this.props.sellers);
    }
  }

  filterImagesLink = images => {
    let _imageList = [];
    for (const prop in images) {
      for (let index = 0; index < images[prop].length; index++) {
        _imageList.push(images[prop][index].link);
      }
    }
    this.imageList = _imageList;
  };

  addSellers = sellers => {
    const _sellers = { ...this.state.sellers };
    sellers.map(
      seller =>
        (_sellers[`${seller.id}`] = {
          name: seller.name,
          points: 0,
          sales: [],
          image: this.getRamdomImage(),
          key: Number(seller.id),
          id: Number(seller.id),
        })
    );
    this.setState({ sellers: _sellers });
  };

  getRamdomImage = () => {
    return this.imageList[Math.floor(Math.random() * this.imageList.length)];
  };

  onSellProduct = key => {
    const sellers = { ...this.state.sellers };
    const points = sellers[key].points + 3;
    const image = this.getRamdomImage();
    sellers[key] = {
      ...sellers[key],
      points,
      image,
    };
    this.setState({ sellers });
    // create invoice
    if (points >= 20) {
      const date = moment().format('YYYY-MM-DD');
      const dueDate = moment()
        .add(7, 'days')
        .format('YYYY-MM-DD');
      const client = 1;
      const items = [{ id: 1, price: 1, quantity: 7 }];
      const seller = key;
      this.props.createInvoice(date, dueDate, client, items, seller);
      this.init();
    }
  };

  init = (invoice = false) => {
    this.imageList = [];
    this.setState({ sellers: {}, searchTerm: '' });
    if (invoice) {
      this.props.resetInvoice();
    }
  };

  render() {
    return (
      <Main>
        <Container>
          <Search value={this.state.searchTerm} onSearch={this.onChangeInputSearch} />
          {this.props.status.status === 'fetching' && <Spinner message={this.props.status.message} />}
          <Sellers sellers={this.state.sellers} onSellProduct={this.onSellProduct} />
          <Invoice invoice={this.props.invoice} resetInvoice={this.props.resetInvoice} />
        </Container>
      </Main>
    );
  }
}

const mapStateToProps = state => ({
  results: state.results,
  status: state.status,
  sellers: state.sellers,
  invoice: state.invoice,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      search,
      setStatus,
      getSellers,
      createInvoice,
      resetInvoice,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
